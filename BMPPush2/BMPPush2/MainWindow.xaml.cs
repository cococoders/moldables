﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using AForge;
using AForge.Imaging;
using AForge.Math;

namespace BMPPush2
{
    public partial class MainWindow : Window
    {
        Grid[] appGrid;
        Canvas[] photoAlbum;
        Canvas[][] appCanvas;
        System.Windows.Controls.Image[,] photo;
        System.Windows.Controls.Image[,] icon;
        System.Windows.Controls.Image focus;
        System.Windows.Controls.Image context;
        System.Windows.Controls.Image tool;
        System.Windows.Controls.Image menu;
        System.Windows.Controls.Image rotated90;
        RenderTargetBitmap[] BlackWhiteBMP;
        Canvas[] BlackWhiteCanvas;
        byte[] blackArray;
        byte[] whiteArray;
        static String[] shapes;
        static String[] items;

        //Define static variables for the CSV reader
        /// <summary>
        /// Class to store one CSV row
        /// </summary>
        public class CsvRow : List<string>
        {
            public string LineText { get; set; }
        }

        /// <summary>
        /// Class to read data from a CSV file
        /// </summary>
        public class CsvFileReader : StreamReader
        {
            public CsvFileReader(Stream stream)
                : base(stream)
            {
            }

            public CsvFileReader(string filename)
                : base(filename)
            {
            }

            /// <summary>
            /// Reads a row of data from a CSV file
            /// </summary>
            /// <param name="row"></param>
            /// <returns></returns>
            public bool ReadRow(CsvRow row)
            {
                row.LineText = ReadLine();
                if (String.IsNullOrEmpty(row.LineText))
                    return false;

                int pos = 0;
                int rows = 0;

                while (pos < row.LineText.Length)
                {
                    string value;

                    // Special handling for quoted field
                    if (row.LineText[pos] == '"')
                    {
                        // Skip initial quote
                        pos++;

                        // Parse quoted value
                        int start = pos;
                        while (pos < row.LineText.Length)
                        {
                            // Test for quote character
                            if (row.LineText[pos] == '"')
                            {
                                // Found one
                                pos++;

                                // If two quotes together, keep one
                                // Otherwise, indicates end of value
                                if (pos >= row.LineText.Length || row.LineText[pos] != '"')
                                {
                                    pos--;
                                    break;
                                }
                            }
                            pos++;
                        }
                        value = row.LineText.Substring(start, pos - start);
                        value = value.Replace("\"\"", "\"");
                    }
                    else
                    {
                        // Parse unquoted value
                        int start = pos;
                        while (pos < row.LineText.Length && row.LineText[pos] != ',')
                            pos++;
                        value = row.LineText.Substring(start, pos - start);
                    }

                    // Add field to list
                    if (rows < row.Count)
                        row[rows] = value;
                    else
                        row.Add(value);
                    rows++;

                    // Eat up to and including next comma
                    while (pos < row.LineText.Length && row.LineText[pos] != ',')
                        pos++;
                    if (pos < row.LineText.Length)
                        pos++;
                }
                // Delete any unused items
                while (row.Count > rows)
                    row.RemoveAt(rows);

                // Return true if any columns read
                return (row.Count > 0);
            }
        }

        static int cleanDisplay;
        static bool clean = true;
        static String filename = "random1.txt";
        static int clicker;
        static int shapeCount = 0;
        static int orientation = 1;
        static int region = 1;

        struct ScreenInfo
        {
            public int nofDisplays;
            public int flexDisplayWidth;
            public int flexDisplayHeight;
            public int nofApps;
            public int[] nofAppScreens;
            public Boolean saveToFile;
            public Boolean renderToScreen;
            public Boolean pushToDisplay;
            public Boolean keepRunning;
        };
        ScreenInfo screenInfo;

        struct Display
        {
            public RenderTargetBitmap bitmapForPush;
            public ImageBrush bitmapForCanvas;
            public Boolean updated;
            public Boolean animating;
            public String server;
            public TcpClient client;
            public NetworkStream stream;
            public StreamWriter streamWriter;
            public bool connected;
            public bool cleanup;
        };
        Display display;

        public MainWindow()
        {
            InitializeComponent();
            /*STEP0: Control everything by key presses*/
            b1.Click += new RoutedEventHandler(b1_Click);
            this.KeyDown += new KeyEventHandler(MainWindow_KeyDown);
            t1.KeyDown += new KeyEventHandler(t1_KeyDown);

            /*STEP1: Initialize Data*/
            int nofApps = 5;
            int i, j;

            screenInfo = new ScreenInfo();
            initializeScreenInfo();

            display = new Display();
            display.bitmapForCanvas = new ImageBrush();
            display.updated = false;
            display.animating = false;
            display.cleanup = false;
            display.server = "10.232.1.11"; //IP address of hummingbird
            display.connected = false;

            appCanvas = new Canvas[screenInfo.nofApps][];
            appGrid = new Grid[screenInfo.nofApps];
            for (i = 0; i < nofApps; i++)
            {
                appGrid[i] = new Grid { Width = screenInfo.flexDisplayWidth, Height = screenInfo.flexDisplayHeight };
                appCanvas[i] = new Canvas[screenInfo.nofAppScreens[i]];
                for(j=0;j<screenInfo.nofAppScreens[i];j++)
                    appCanvas[i][j] = new Canvas { Width = screenInfo.flexDisplayWidth, Height = screenInfo.flexDisplayHeight };
            }
            icon = new System.Windows.Controls.Image[2, 5];

            BlackWhiteBMP = new RenderTargetBitmap[2];
            BlackWhiteCanvas = new Canvas[2];
            BlackWhiteCanvas[0] = new Canvas { Width = screenInfo.flexDisplayWidth, Height = screenInfo.flexDisplayHeight };
            BlackWhiteCanvas[1] = new Canvas { Width = screenInfo.flexDisplayWidth, Height = screenInfo.flexDisplayHeight };
            photoAlbum = new Canvas[2];
            photoAlbum[0] = new Canvas { Width = screenInfo.flexDisplayWidth, Height = screenInfo.flexDisplayHeight };
            photoAlbum[1] = new Canvas { Width = screenInfo.flexDisplayWidth, Height = screenInfo.flexDisplayHeight };
            photo = new System.Windows.Controls.Image[2, 7];

            /*STEP2: Initialize UI*/
            buildBlackWhite();
            createBlackWhiteBitmap();

            buildDocApp(ref appGrid[0]);
            buildEmailApp(ref appCanvas[2], screenInfo.nofAppScreens[2]);
            buildPhotoAlbum();
            buildPhotoApp(ref appGrid[2]);
            buildMapApp(ref appGrid[3]);
            buildSearchApp(ref appGrid[4]);
            buildMenuApp(ref appCanvas[0], screenInfo.nofAppScreens[0]);
            buildMoldableApp(ref appCanvas[3][0], screenInfo.nofAppScreens[3]);

            //Get all the shapes from the csv file and put them into arrays
            ReadTest();
            //readItems();  // read the first row of items and add to the items array

            /*STEP3: Create a timer that updates the Bitmap & the UI with new info every 250ms*/
            System.Windows.Threading.DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(200);
            timer.Tick += new EventHandler(delegate(object s, EventArgs a)
            {
                for (i = 0; i < screenInfo.nofDisplays; i++)
                {
                    if (display.updated || display.animating)
                    {
                        display.updated = false;
                            sendBitmap(display); //This function sends the bitmap of the UI to each display
                        if(screenInfo.renderToScreen)
                            renderUI(display); //This function shows the created bitmap on the screen
                        if (screenInfo.saveToFile)
                            saveToPng(display); //save to png locally. for debugging purposes only
                    }
                }
            });
            timer.Start();

            System.Windows.Threading.DispatcherTimer timer2 = new System.Windows.Threading.DispatcherTimer();
            timer2.Interval = TimeSpan.FromMilliseconds(10000);
            timer2.Tick += new EventHandler(timer2_Tick);
            timer2.Start();
        }

        void t1_KeyDown(object sender, KeyEventArgs e)
        {
            //throw new NotImplementedException();
            if (e.Key.ToString() == "Return")
                executeCommand();
                //b1_Click(sender, e);
        }

        void timer2_Tick(object sender, EventArgs e)
        {
            if (cleanDisplay<1 && clean)
                cleanDisplay = 3;
        }

        void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key.ToString())
            {
                case "F12":
                    Connect(ref display); //try to connect to the hummingbird
                    break;
                case "Q":
                case "q": Application.Current.Shutdown();
                    break;
                default:
                    break;
            }
        }

        //Jesse & David Holman
        //static void Connect(String server, String message)
        //Modified: Aneesh
        void Connect(ref Display d)
        {
            try
            {
                Int32 port = 8888;
                d.client = new TcpClient(d.server, port);
                Byte[] data = System.Text.Encoding.ASCII.GetBytes("init ");
                d.stream = d.client.GetStream();
                d.stream.Write(data, 0, data.Length);
                d.stream.Flush();
                d.connected = true;
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("ArgumentNullException: {0}", e);
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
        }

        // This function creates two image arrays
        void createBlackWhiteBitmap()
        {
            int screenWidth = 1280;
            int screenHeight = 960;
            int totalSize = screenWidth * screenHeight;

            System.Drawing.Bitmap bmpScreenShot1 = new System.Drawing.Bitmap(screenWidth, screenHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            System.Drawing.Bitmap bmpScreenShot2 = new System.Drawing.Bitmap(screenWidth, screenHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            TransformedBitmap flippedTargetBmp1, flippedTargetBmp2;
            
            flippedTargetBmp1 = new TransformedBitmap(BlackWhiteBMP[0], new RotateTransform(90));
            bmpScreenShot1 = BitmapSourceToBitmap2(flippedTargetBmp1);

            flippedTargetBmp2 = new TransformedBitmap(BlackWhiteBMP[1], new RotateTransform(90));
            bmpScreenShot2 = BitmapSourceToBitmap2(flippedTargetBmp2);

            System.Drawing.Bitmap grey1 = AForge.Imaging.Filters.Grayscale.CommonAlgorithms.RMY.Apply(bmpScreenShot1);
            System.Drawing.Bitmap grey2 = AForge.Imaging.Filters.Grayscale.CommonAlgorithms.RMY.Apply(bmpScreenShot2);

            // Dither hither
            AForge.Imaging.Filters.BayerDithering filter = new AForge.Imaging.Filters.BayerDithering();
            filter.ApplyInPlace(grey1);
            filter.ApplyInPlace(grey2);

            // Convert to Byte Array
            byte[] bmpArray1 = ToByteArray(grey1, System.Drawing.Imaging.ImageFormat.Bmp);
            blackArray = new byte[totalSize];

            byte[] bmpArray2 = ToByteArray(grey2, System.Drawing.Imaging.ImageFormat.Bmp);
            whiteArray = new byte[totalSize];

            // Bitmaps are stored with rows reversed, also deleting header info (always 1078 bytes regardess of image size)
            for (int y = 0; y < screenHeight; y++)
            {
                Array.Copy(bmpArray1, 1078 + ((screenHeight - (y + 1)) * screenWidth), blackArray, y * screenWidth, screenWidth);
                Array.Copy(bmpArray2, 1078 + ((screenHeight - (y + 1)) * screenWidth), whiteArray, y * screenWidth, screenWidth);
            }
        }

        void sendBitmap(Display d)
        {
            int screenWidth = 1280;
            int screenHeight = 960;
            int totalSize = screenWidth * screenHeight;

            /*if (d.connected)
            {
                d.stream.Write(whiteArray, 0, whiteArray.Length);
                d.stream.Write(blackArray, 0, blackArray.Length);
                d.stream.Write(whiteArray, 0, whiteArray.Length);
                d.stream.Flush();
            }*/

            System.Drawing.Bitmap bmpScreenShot = new System.Drawing.Bitmap(screenWidth, screenHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            TransformedBitmap flippedTargetBmp;
            flippedTargetBmp = new TransformedBitmap(display.bitmapForPush, new RotateTransform(90));

            //to clean the display, send  black / white / black. 
            if (d.cleanup)
            {
                if ((cleanDisplay == 3 || cleanDisplay == 1))
                {
                    //System.Console.Write("-Black-");
                    flippedTargetBmp = new TransformedBitmap(BlackWhiteBMP[1], new RotateTransform(90));
                }
                else if (cleanDisplay == 2)
                {
                    //System.Console.WriteLine(DateTime.Now.ToShortTimeString());
                    //System.Console.Write("-White-");
                    flippedTargetBmp = new TransformedBitmap(BlackWhiteBMP[0], new RotateTransform(90));
                }
                else if (cleanDisplay < 1)
                {
                    //System.Console.Write("-Regular-");
                    //flippedTargetBmp = new TransformedBitmap(display[i].bitmapForPush, new RotateTransform(90));
                }
                cleanDisplay--;
            }
            bmpScreenShot = BitmapSourceToBitmap2(flippedTargetBmp);
            // Greyscale that shiz
            //AForge.Imaging.Filters.Grayscale.CommonAlgorithms.BT709
            //AForge.Imaging.Filters.Grayscale.CommonAlgorithms.RMY
            //AForge.Imaging.Filters.Grayscale.CommonAlgorithms.Y
            System.Drawing.Bitmap grey = AForge.Imaging.Filters.Grayscale.CommonAlgorithms.Y.Apply(bmpScreenShot);

            //grey.Save(i.ToString() + "step3.bmp");

            // Dither hither
            //AForge.Imaging.Filters.BayerDithering filter = new AForge.Imaging.Filters.BayerDithering();
            //filter.ApplyInPlace(grey);

            //grey.Save(i.ToString() + "step4.bmp");

            // Convert to Byte Array
            byte[] bmpArray = ToByteArray(grey, System.Drawing.Imaging.ImageFormat.Bmp);
            byte[] bwArray = new byte[totalSize];

            // Bitmaps are stored with rows reversed, also deleting header info (always 1078 bytes regardess of image size)
            for (int y = 0; y < screenHeight; y++)
            {
                Array.Copy(bmpArray, 1078+((screenHeight-(y+1)) * screenWidth), bwArray, y*screenWidth, screenWidth);
            }

            // Send over network
            if (d.connected)
            {
                    d.stream.Write(bwArray, 0, bwArray.Length);
                    d.stream.Flush();
            }
        }

        //Obsolete method, can probably be deleted
        void changeUI(int appNum, int screenNum)
        {
        }

        void executeCommand()
        {
            string input = t1.Text;
            string[] values = input.Split(',');
            int x = Convert.ToInt32(values[0]);
            int y = Convert.ToInt32(values[1]);
            int a = Convert.ToInt32(values[2]);
            int b = Convert.ToInt32(values[3]);

            setUI(x, y, a, b);
            
            display.bitmapForPush = renderBitmap(appCanvas[3][0]);
            display.bitmapForCanvas.ImageSource = display.bitmapForPush;
            display.updated = true;
        }

        void b1_Click(object sender, RoutedEventArgs e)
        {
            int counts = clicks();
            int UIelement = Convert.ToInt32(items[counts]); // Now we know the correct UI element

            int orientation = getOrientation();

            int region = getRegion();

            int shape = getShape();
            int shapeToCall = 1;

            // Map the shape numbers to the coded shapes
            switch (shape)
            {
                case 1:
                case 6: shapeToCall = 1;
                        break;
                case 2:
                case 7: shapeToCall = 2;
                        break;
                case 3:
                case 8: shapeToCall = 3;
                        break;
                case 4:
                case 9: shapeToCall = 4;
                        break;
                case 5:
                case 10: shapeToCall = 5;
                         break;
                case 11:
                case 12: shapeToCall = 6;
                         break;
                case 13:
                case 14: shapeToCall = 7;
                         break;
            }

            setUI(shapeToCall, orientation, region, UIelement);
            display.bitmapForPush = renderBitmap(appCanvas[3][0]);
            display.bitmapForCanvas.ImageSource = display.bitmapForPush;
            display.updated = true;


            //Test to see if we need to increment
            if (counts > 0 && (counts+1) % 4 == 0)
            {
                //increment the region          
                setRegion(shapeToCall);

                //if the region just went back to 1, then increment the orientation
                if (getRegion() == 1)
                {
                    setOrientation();
                    // if the orientation just went back to 1, that means increment the shape
                    if (getOrientation() == 1)
                        setShape();
                }
            }

        }

        // This will read the first line of the csv and put it in the shapes array
        void ReadTest()
        {
            shapes = new String[14];
            items = new String[272];

            // Read sample data from CSV file
            using (CsvFileReader reader = new CsvFileReader(filename))
            {
                CsvRow row = new CsvRow();
                reader.ReadRow(row);
                int i = 0;
                foreach (string s in row)
                {
                    shapes[i] = s;
                    i++;
                } 
               
                int k = 0;
                while (k < items.Length)
                {
                    reader.ReadRow(row);
                    foreach (string s in row)
                    {
                        items[k] = s;
                        k++;
                    }
                }
            }
        }

        static int clicks()
        {
            return clicker++;
        }

        static int getShape()
        {
            int shape = Convert.ToInt32(shapes[shapeCount]);
            return shape;
        }

        static void setShape()
        {
            shapeCount++;
        }

        static int getOrientation()
        {
            return orientation;
        }

        static void setOrientation()
        {
            if (orientation == 1)
                orientation = 2;
            else if (orientation == 2)
                orientation = 1;

        }
        static int getRegion()
        {
            return region;
        }

        static void setRegion(int type)
        {
            region++;
            if (type == 4 || type == 5 || type == 7)
            {
                if (region == 4)
                    region = 1;
            }
            else
            {
                if (region == 3)
                    region = 1;
            }
        }

        //Render to the computer screen
        void renderUI(Display d)
        {
            c1.Background = d.bitmapForCanvas;
        }

        //Set some variables
        void initializeScreenInfo()
        {
            screenInfo.flexDisplayWidth = 960;
            screenInfo.flexDisplayHeight = 1280;
            screenInfo.nofDisplays = 1;
            screenInfo.nofApps = 6;     // Changed from 5 to 6 to accommodate Discrete App
            screenInfo.nofAppScreens = new int[screenInfo.nofApps];
            screenInfo.saveToFile = false;
            screenInfo.renderToScreen = true;
            screenInfo.pushToDisplay = true;
            screenInfo.keepRunning = true;

            screenInfo.nofAppScreens[0] = 2; //Menu App
            screenInfo.nofAppScreens[1] = 10; //Doc App = 75
            screenInfo.nofAppScreens[2] = 11; //Email App
            screenInfo.nofAppScreens[3] = 2; //Photo App
            screenInfo.nofAppScreens[4] = 2; //Map App
        }

        void buildBlackWhite()
        {
            System.Windows.Controls.Image pic1 = new System.Windows.Controls.Image();
            System.Windows.Controls.Image pic2 = new System.Windows.Controls.Image();
            pic1.Source = new BitmapImage(new Uri(@"../../MiscImages/black_blank.jpg", UriKind.RelativeOrAbsolute));
            pic2.Source = new BitmapImage(new Uri(@"../../MiscImages/white_blank.jpg", UriKind.RelativeOrAbsolute));
            BlackWhiteCanvas[0].Children.Add(pic1);
            BlackWhiteCanvas[1].Children.Add(pic2);

            for (int i = 0; i < 2; i++)
            {
                BlackWhiteCanvas[i].Measure(new Size(BlackWhiteCanvas[i].Width, BlackWhiteCanvas[i].Height));
                BlackWhiteCanvas[i].Arrange(new Rect(new Size(BlackWhiteCanvas[i].Width, BlackWhiteCanvas[i].Height)));
                BlackWhiteCanvas[i].UpdateLayout();
            }
            
            BlackWhiteBMP[0] = renderBitmap(BlackWhiteCanvas[0]);
            BlackWhiteBMP[1] = renderBitmap(BlackWhiteCanvas[1]);
        }

    /* --- DP: Here is the Moldable App builder (initializer) --- */
        
        void buildMoldableApp(ref Canvas app, int nofScreens)
        {

            double focusWidth, focusHeight, focusTop, focusLeft;
 
            focusWidth = 750;
            focusHeight = 500;
            focusTop = 200;
            focusLeft = 100;
             
            focus = new System.Windows.Controls.Image();
            focus.Source = new BitmapImage(new Uri(@"../../MiscImages/Moldable/picture_1.jpg", UriKind.RelativeOrAbsolute));
            focus.Width = focusWidth;
            focus.Height = focusHeight;
            focus.SetValue(Canvas.LeftProperty, focusLeft);
            focus.SetValue(Canvas.TopProperty, focusTop);
            app.Children.Add(focus);

            app.Background = Brushes.White;
            app.Measure(new Size(app.Width, app.Height));
            app.Arrange(new Rect(new Size(app.Width, app.Height)));
            app.UpdateLayout();
        }

        void setUI(int shape, int orientation, int region, int type)
        {
            appCanvas[3][0].Children.Clear();

            /* --- screenWidth = 960;
                   screenHeight = 1280; --- */
            int regionHeight = 0;
            int regionWidth = 0;
            int regionStartTop = 0;
            int regionMaxTop = 0;
            int regionStartLeft = 0; 
            int regionMaxLeft = 0;
            bool rotate = false;

            // One curve, top 1/4, bottom 3/4
            if (shape == 1)
            {
                if (region == 1)
                {
                    regionHeight = 310;
                    regionWidth = 950;
                    regionStartTop = 10;
                    regionStartLeft = 10;
                }
                else
                {
                    regionHeight = 950;
                    regionWidth = 950;
                    regionStartTop = 330;
                    regionStartLeft = 10;
                }       
            }
            else if (shape == 2) // One curve, top 1/2, bottom 1/2
            {
                if (region == 1)
                {
                    regionHeight = 630;
                    regionWidth = 950;
                    regionStartTop = 10;
                    regionStartLeft = 10;
                }
                else
                {
                    regionHeight = 630;
                    regionWidth = 950;
                    regionStartTop = 650;
                    regionStartLeft = 10;
                }
            }

            else if (shape == 3) // One curve, top 3/4, bottom 1/4
            {
                if (region == 1)
                {
                    regionHeight = 950;
                    regionWidth = 950;
                    regionStartTop = 10;
                    regionStartLeft = 10;
                }
                else
                {
                    regionHeight = 310;
                    regionWidth = 950;
                    regionStartTop = 970;
                    regionStartLeft = 10;
                }
            }

            else if (shape == 4) // Wide concave curve divided into 3 equal portions
            {
                if (region == 1)
                {
                    regionHeight = 417;
                    regionWidth = 950;
                    regionStartTop = 10;
                    regionStartLeft = 10;
                }
                else if (region == 2)
                {
                    regionHeight = 417;
                    regionWidth = 950;
                    regionStartTop = 437;
                    regionStartLeft = 10;
                }

                else if (region == 3)
                {
                    regionHeight = 417;
                    regionWidth = 950;
                    regionStartTop = 863;
                    regionStartLeft = 10;
                }
            }

            else if (shape == 5) // Wide concave curve 1/4 on each side and 1/2 in the middle
            {
                if (region == 1)
                {
                    regionHeight = 310;
                    regionWidth = 950;
                    regionStartTop = 10;
                    regionStartLeft = 10;
                }
                else if (region == 2)
                {
                    regionHeight = 630;
                    regionWidth = 950;
                    regionStartTop = 330;
                    regionStartLeft = 10;
                }

                else if (region == 3)
                {
                    regionHeight = 310;
                    regionWidth = 950;
                    regionStartTop = 970;
                    regionStartLeft = 10;
                }
            }

            else if (shape == 6) // Portrait book, 1/2 and 1/2, one fold
            {
                if (region == 1)
                {
                    regionHeight = 1270;
                    regionWidth = 470;
                    regionStartTop = 10;
                    regionStartLeft = 10;
                }
                else if (region == 2)
                {
                    regionHeight = 1270;
                    regionWidth = 470;
                    regionStartTop = 10;
                    regionStartLeft = 490;
                }
            }
     
            else if (shape == 7) // Portrait Concave curve, divided into 3 1/3 parts
            {
                if (region == 1)
                {
                    regionHeight = 1270;
                    regionWidth = 310;
                    regionStartTop = 10;
                    regionStartLeft = 10;
                }
                else if (region == 2)
                {
                    regionHeight = 1270;
                    regionWidth = 310;
                    regionStartTop = 10;
                    regionStartLeft = 330;
                }
                else if (region == 3)
                {
                    regionHeight = 1270;
                    regionWidth = 310;
                    regionStartTop = 10;
                    regionStartLeft = 650;
                }
            }

            if (orientation == 1)
                rotate = true;

            regionMaxTop = regionStartTop + regionHeight;
            regionMaxLeft = regionStartLeft + regionWidth;

            if (type == 1)
                setContext(regionStartTop, regionStartLeft, regionMaxTop, regionMaxLeft, rotate);
            else if (type == 2)
                setFocus(regionStartTop, regionStartLeft, regionHeight, regionWidth, rotate);
            else if (type == 3)
                setMenu(regionStartTop, regionStartLeft, regionMaxTop, regionMaxLeft, regionHeight, regionWidth, rotate);
            else
                setTools(regionStartTop, regionStartLeft, regionMaxTop, regionMaxLeft, regionHeight, regionWidth, rotate);
        }

        void setContext(int startTopparam, int startLeftparam, int maxTopparam, int maxLeftparam, bool rotate) 
        {
            double maxLeft = maxLeftparam;
            double maxTop = maxTopparam;
            double top = startTopparam;
            double left = startLeftparam;
            int picNumber = 1;


            while (left < maxLeft && top < maxTop - 80)
            {
                context = new System.Windows.Controls.Image();
                if (rotate)
                {
                    rotated90 = new System.Windows.Controls.Image();
                    rotated90.Width = 80;
                    rotated90.Height = 110;
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    bi.UriSource = new Uri(@"../../MiscImages/Moldable/picture_" + picNumber + ".jpg", UriKind.RelativeOrAbsolute);
                    bi.Rotation = Rotation.Rotate90;
                    bi.EndInit();
                    rotated90.Source = bi;
                    context = rotated90;

                    context.SetValue(Canvas.LeftProperty, left);
                    context.SetValue(Canvas.TopProperty, top);
                    appCanvas[3][0].Children.Add(context);
                    left = left + 95;
                    if (left >= maxLeft - 40)
                    {
                        left = startLeftparam;
                        top = top + 120;
                    }

                }
                else
                {
                    context.Source = new BitmapImage(new Uri(@"../../MiscImages/Moldable/picture_" + picNumber + ".jpg", UriKind.RelativeOrAbsolute));
                    context.Width = 110;
                    context.Height = 80;
                    context.SetValue(Canvas.LeftProperty, left);
                    context.SetValue(Canvas.TopProperty, top);
                    appCanvas[3][0].Children.Add(context);
                    left = left + 130;
                    if (left >= maxLeft - 80)
                    {
                        left = startLeftparam;
                        top = top + 90;
                    }
                }
                picNumber++;
            }

            appCanvas[3][0].Background = Brushes.White;
            appCanvas[3][0].Measure(new Size(appCanvas[3][0].Width, appCanvas[3][0].Height));
            appCanvas[3][0].Arrange(new Rect(new Size(appCanvas[3][0].Width, appCanvas[3][0].Height)));
            appCanvas[3][0].UpdateLayout();
        }

        void setFocus(int startTopparam, int startLeftparam, int regionHeight, int regionWidth, bool rotate)
        {
            double focusWidth = regionWidth - 20;
            double focusHeight = regionHeight;
            double focusTop = startTopparam;
            double focusLeft = startLeftparam;

            focus = new System.Windows.Controls.Image();

            if (rotate)
            {
                rotated90 = new System.Windows.Controls.Image();
                rotated90.Width = focusWidth;
                rotated90.Height = focusHeight -10;
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.UriSource = new Uri(@"../../MiscImages/Moldable/picture_1.jpg", UriKind.RelativeOrAbsolute);
                bi.Rotation = Rotation.Rotate90;
                bi.EndInit();
                rotated90.Source = bi;
                focus = rotated90;
                focus.SetValue(Canvas.LeftProperty, focusLeft);
                focus.SetValue(Canvas.TopProperty, focusTop);
                appCanvas[3][0].Children.Add(focus);
            }
            else
            {
                focus.Source = new BitmapImage(new Uri(@"../../MiscImages/Moldable/picture_1.jpg", UriKind.RelativeOrAbsolute));
                focus.Width = focusWidth;
                focus.Height = focusHeight;
                focus.SetValue(Canvas.LeftProperty, focusLeft);
                focus.SetValue(Canvas.TopProperty, focusTop);
                appCanvas[3][0].Children.Add(focus);
            }
            appCanvas[3][0].Background = Brushes.White;
            appCanvas[3][0].Measure(new Size(appCanvas[3][0].Width, appCanvas[3][0].Height));
            appCanvas[3][0].Arrange(new Rect(new Size(appCanvas[3][0].Width, appCanvas[3][0].Height)));
            appCanvas[3][0].UpdateLayout();
        }

        void setMenu(int startTopparam, int startLeftparam, int maxTopparam, int maxLeftparam, int regionHeight, int regionWidth, bool rotate)
        {
            double maxLeft = (double)maxLeftparam;
            double flipStartLeft = (double)maxLeftparam - 40;;
            double maxTop = (double)maxTopparam;
            double top = (double)startTopparam;
            double left = (double)startLeftparam;
            double flipMaxLeft = (double)startLeftparam;
            int item = 1;

            //if we don't want to rotate, handle menus accordingly
            if (!rotate)
            {
                //If the region is taller than it is Wide, make a vertical menu
                if (regionWidth < regionHeight)
                {
                    while (top < maxTop - 80)
                    {
                        menu = new System.Windows.Controls.Image();
                        menu.Source = new BitmapImage(new Uri(@"../../MiscImages/Moldable/Menu_" + item + ".jpg", UriKind.RelativeOrAbsolute));
                        menu.Width = 120;
                        menu.Height = 36;
                        menu.SetValue(Canvas.LeftProperty, left);
                        menu.SetValue(Canvas.TopProperty, top);
                        appCanvas[3][0].Children.Add(menu);
                        top = top + 60;
                        item++;
                    }
                }
                else  //make a horizontal menu
                {
                    while (left < maxLeft - 60)
                    {
                        menu = new System.Windows.Controls.Image();
                        menu.Source = new BitmapImage(new Uri(@"../../MiscImages/Moldable/Menu_" + item + ".jpg", UriKind.RelativeOrAbsolute));
                        menu.SetValue(Canvas.LeftProperty, left);
                        menu.SetValue(Canvas.TopProperty, top);
                        appCanvas[3][0].Children.Add(menu);
                        left = left + menu.Source.Width + 5;
                        item++;
                    }
                }
            }
            else //it needs to rotate because orientation == 2
            {
                //Make a vertical menu with descending Left coordinates starting at maximum Left coordinates
                if (regionWidth > regionHeight)
                {
                    while (flipStartLeft > flipMaxLeft + 60)
                    {
                        rotated90 = new System.Windows.Controls.Image();
                        rotated90.Width = 36;
                        rotated90.Height = 120;
                        BitmapImage bi = new BitmapImage();
                        bi.BeginInit();
                        bi.UriSource = new Uri(@"../../MiscImages/Moldable/Menu_" + item + ".jpg", UriKind.RelativeOrAbsolute);
                        bi.Rotation = Rotation.Rotate90;
                        bi.EndInit();
                        rotated90.Source = bi;
                        menu = rotated90;
                        menu.SetValue(Canvas.LeftProperty, flipStartLeft);
                        menu.SetValue(Canvas.TopProperty, top);
                        appCanvas[3][0].Children.Add(menu);
                        flipStartLeft = flipStartLeft - 40;
                        item++;
                    }
                }
                else  //make a horizontal menu with descending top coordinates, starting at maximum left coordinates
                {
                    while (top < maxTop - 80)
                    {
                        rotated90 = new System.Windows.Controls.Image();
                        rotated90.Width = 36;
                        rotated90.Height = 120;
                        BitmapImage bi = new BitmapImage();
                        bi.BeginInit();
                        bi.UriSource = new Uri(@"../../MiscImages/Moldable/Menu_" + item + ".jpg", UriKind.RelativeOrAbsolute);
                        bi.Rotation = Rotation.Rotate90;
                        bi.EndInit();
                        rotated90.Source = bi;
                        menu = rotated90;
                        menu.SetValue(Canvas.LeftProperty, flipStartLeft);
                        menu.SetValue(Canvas.TopProperty, top);
                        appCanvas[3][0].Children.Add(menu);
                        top = top + 110;
                        item++;
                    }
                }
            }
            appCanvas[3][0].Background = Brushes.White;
            appCanvas[3][0].Measure(new Size(appCanvas[3][0].Width, appCanvas[3][0].Height));
            appCanvas[3][0].Arrange(new Rect(new Size(appCanvas[3][0].Width, appCanvas[3][0].Height)));
            appCanvas[3][0].UpdateLayout();
        }

        void setTools(int startTopparam, int startLeftparam, int maxTopparam, int maxLeftparam, int regionHeight, int regionWidth, bool rotate)
        {
            double maxLeft = (double)maxLeftparam;
            double maxTop = (double)maxTopparam;
            double top = (double)startTopparam;
            double left = (double)startLeftparam;
            int item = 1;

            //The following variables are needed when orientation == 2
            double flipStartLeft = (double)maxLeftparam - 80;
            double flipMaxLeft = (double)startLeftparam;

            //if we don't want to rotate, handle tools normally
            if (!rotate)
            {
                //If the region is taller than it is Wide, make a vertical toolbar
                if (regionWidth < regionHeight)
                {
                    while (top < maxTop && left < maxLeft)
                    {
                        tool = new System.Windows.Controls.Image();
                        tool.Source = new BitmapImage(new Uri(@"../../MiscImages/Moldable/tool_" + item + ".png", UriKind.RelativeOrAbsolute));
                        tool.Width = 70;
                        tool.Height = 70;
                        tool.SetValue(Canvas.LeftProperty, left);
                        tool.SetValue(Canvas.TopProperty, top);
                        appCanvas[3][0].Children.Add(tool);
                        top = top + 90;

                        if (top >= maxTop - 80)
                        {
                            top = startTopparam;
                            left = left + 90;
                        }

                        item++;
                    }
                }
                else // make a horizontal toolbar
                {
                    while (top < maxTop - 60 && left < maxLeft)
                    {
                        tool = new System.Windows.Controls.Image();
                        tool.Source = new BitmapImage(new Uri(@"../../MiscImages/Moldable/tool_" + item + ".png", UriKind.RelativeOrAbsolute));
                        tool.Width = 70;
                        tool.Height = 70;
                        tool.SetValue(Canvas.LeftProperty, left);
                        tool.SetValue(Canvas.TopProperty, top);
                        appCanvas[3][0].Children.Add(tool);
                        left = left + 90;

                        if (left >= maxLeft - 60)
                        {
                            left = startLeftparam;
                            top = top + 90;
                        }
                        item++;
                    }
                }
            }
            else   //the toolbar needs to rotate because orientation == 2
            {
                //Make a vertical toolbar with descending Left coordinates starting at maximum Left coordinates
                if (regionWidth > regionHeight)
                {
                    while (flipStartLeft > flipMaxLeft && top < maxTop)
                    {
                        rotated90 = new System.Windows.Controls.Image();
                        rotated90.Width = 70;
                        rotated90.Height = 70;
                        BitmapImage bi = new BitmapImage();
                        bi.BeginInit();
                        bi.UriSource = new Uri(@"../../MiscImages/Moldable/tool_" + item + ".png", UriKind.RelativeOrAbsolute);
                        bi.Rotation = Rotation.Rotate90;
                        bi.EndInit();
                        rotated90.Source = bi;
                        tool = rotated90;
                        tool.SetValue(Canvas.LeftProperty, flipStartLeft);
                        tool.SetValue(Canvas.TopProperty, top);
                        appCanvas[3][0].Children.Add(tool);
                        flipStartLeft = flipStartLeft - 90;

                        if (flipStartLeft <= flipMaxLeft + 40)
                        {
                            flipStartLeft = maxLeftparam;
                            top = top + 90;
                        }
                        item++;
                    }
                }
                else  //make a Horizontal menu with descending top coordinates, starting at maximum left coordinates
                {
                    while (top < maxTop && flipStartLeft > flipMaxLeft)
                    {
                        rotated90 = new System.Windows.Controls.Image();
                        rotated90.Width = 70;
                        rotated90.Height = 70;
                        BitmapImage bi = new BitmapImage();
                        bi.BeginInit();
                        bi.UriSource = new Uri(@"../../MiscImages/Moldable/tool_" + item + ".png", UriKind.RelativeOrAbsolute);
                        bi.Rotation = Rotation.Rotate90;
                        bi.EndInit();
                        rotated90.Source = bi;
                        tool = rotated90;
                        tool.SetValue(Canvas.LeftProperty, flipStartLeft);
                        tool.SetValue(Canvas.TopProperty, top);
                        appCanvas[3][0].Children.Add(tool);
                        top = top + 90;

                        if (top >= maxTop - 80)
                        {
                            top = startTopparam;
                            flipStartLeft = flipStartLeft - 90;
                        }

                        item++;
                    }
                }
            }
            appCanvas[3][0].Background = Brushes.White;
            appCanvas[3][0].Measure(new Size(appCanvas[3][0].Width, appCanvas[3][0].Height));
            appCanvas[3][0].Arrange(new Rect(new Size(appCanvas[3][0].Width, appCanvas[3][0].Height)));
            appCanvas[3][0].UpdateLayout();
        }

        //sample app
        void buildMenuApp(ref Canvas[] app, int nofScreens)
        {
            int i, j, k, a, b;
            int width, height;
            double[,] gridPos = new double[6, 2];

            width = screenInfo.flexDisplayWidth / 2;
            height = (screenInfo.flexDisplayHeight / 3 ) - 20;

            //for (i = 0; i < nofScreens; i++)
            //    app[i] = new Canvas { Width = screenInfo.flexDisplayWidth, Height = screenInfo.flexDisplayHeight };

            for (i = 0, k = 0; i < 3; i++)
            {
                for (j = 0; j < 2; j++)
                {
                    gridPos[k, 0] = (width * j);
                    gridPos[k++, 1] = (height * i);
                }
            }

            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 4; j++)
                {
                    icon[i, j] = new System.Windows.Controls.Image();
                    a = i + 1;
                    b = j + 1;
                    icon[i, j].Source = new BitmapImage(new Uri(@"../../Icons/icon0" + a + "_0" + b + ".jpg", UriKind.RelativeOrAbsolute));
                    icon[i, j].Width = width;
                    icon[i, j].Height = height;
                    icon[i, j].SetValue(Canvas.LeftProperty, gridPos[j, 0]);
                    icon[i, j].SetValue(Canvas.TopProperty, gridPos[j, 1]+20);
                    app[i].Children.Add(icon[i, j]);
                }
            }
            i = 0;
            j = 4;
            icon[i, j] = new System.Windows.Controls.Image();
            a = i + 1; b = j + 1;
            icon[i, j].Source = new BitmapImage(new Uri(@"../../Icons/icon0" + a + "_0" + b + ".jpg", UriKind.RelativeOrAbsolute));
            icon[i, j].Width = width;
            icon[i, j].Height = height;
            icon[i, j].SetValue(Canvas.LeftProperty, gridPos[j, 0]);
            icon[i, j].SetValue(Canvas.TopProperty, gridPos[j, 1]+20);
            app[i].Children.Add(icon[i, j]);

            app[0].Background = Brushes.White;
            app[0].Measure(new Size(app[0].Width, app[0].Height));
            app[0].Arrange(new Rect(new Size(app[0].Width, app[0].Height)));
            app[0].UpdateLayout();
            app[1].Background = Brushes.White;
            app[1].Measure(new Size(app[1].Width, app[1].Height));
            app[1].Arrange(new Rect(new Size(app[1].Width, app[1].Height)));
            app[1].UpdateLayout();
        }

        //sample app
        void buildDocApp(ref Grid grid)
        {
            var text = new TextBlock
            {
                Text = "Document App",
                FontSize = 100,
                FontWeight =
                FontWeights.Bold,
                Background = Brushes.White
            };
            grid.Children.Add(text);
            grid.Measure(new Size(grid.Width, grid.Height));
            grid.Arrange(new Rect(new Size(grid.Width, grid.Height)));
            grid.UpdateLayout();
        }

        //sample app
        void buildEmailApp(ref Canvas[] app, int nofScreens)
        {
            System.Windows.Controls.Image pic = new System.Windows.Controls.Image();
            pic.Source = new BitmapImage(new Uri(@"../../Email/email1.jpg", UriKind.RelativeOrAbsolute));
            app[0].Children.Add(pic);
            //app[0].Background = Brushes.White;
            app[0].Measure(new Size(app[0].Width, app[0].Height));
            app[0].Arrange(new Rect(new Size(app[0].Width, app[0].Height)));
            app[0].UpdateLayout();

        /* --- DP: Here I will include more images from the Email folder and arrange them in the Canvas[] array --- */

            pic = new System.Windows.Controls.Image();
            pic.Source = new BitmapImage(new Uri(@"../../Email/email2.jpg", UriKind.RelativeOrAbsolute));
            app[1].Children.Add(pic);
            //app[0].Background = Brushes.White;
            app[1].Measure(new Size(app[1].Width, app[1].Height));
            app[1].Arrange(new Rect(new Size(app[1].Width, app[1].Height)));
            app[1].UpdateLayout();
       
        }
        
        //sample app
        void buildPhotoAlbum()
        {
            int i, j, k, a, b;
            double[,] gridPos = new double[12, 2];
            for (i = 0, k = 0; i < 4; i++)
            {
                for (j = 0; j < 3; j++)
                {
                    gridPos[k, 0] = (320 * j);
                    gridPos[k++, 1] = (320 * i);
                }
            }

            for (i = 0; i < 2; i++)
            {
                for (j = 0; j < 6; j++)
                {
                    photo[i, j] = new System.Windows.Controls.Image();
                    a = i + 1;
                    b = j + 1;
                    photo[i, j].Source = new BitmapImage(new Uri(@"../../Album/album0" + a + "_0" + b + ".jpg", UriKind.RelativeOrAbsolute));
                    photo[i, j].Width = 320;
                    photo[i, j].Height = 320;
                    photo[i, j].SetValue(Canvas.LeftProperty, gridPos[j, 0]);
                    photo[i, j].SetValue(Canvas.TopProperty, gridPos[j, 1]);
                    photoAlbum[i].Children.Add(photo[i, j]);
                }
            }
            i = 1;
            j = 6;
            photo[i, j] = new System.Windows.Controls.Image();
            a = i + 1; b = j + 1;
            photo[i, j].Source = new BitmapImage(new Uri(@"../../Album/album0" + a + "_0" + b + ".jpg", UriKind.RelativeOrAbsolute));
            photo[i, j].Width = 320;
            photo[i, j].Height = 320;
            photo[i, j].SetValue(Canvas.LeftProperty, gridPos[j, 0]);
            photo[i, j].SetValue(Canvas.TopProperty, gridPos[j, 1]);
            photoAlbum[i].Children.Add(photo[i, j]);

            photoAlbum[0].Background = Brushes.White;
            photoAlbum[0].Measure(new Size(photoAlbum[0].Width, photoAlbum[0].Height));
            photoAlbum[0].Arrange(new Rect(new Size(photoAlbum[0].Width, photoAlbum[0].Height)));
            photoAlbum[0].UpdateLayout();
            photoAlbum[1].Background = Brushes.White;
            photoAlbum[1].Measure(new Size(photoAlbum[1].Width, photoAlbum[1].Height));
            photoAlbum[1].Arrange(new Rect(new Size(photoAlbum[1].Width, photoAlbum[1].Height)));
            photoAlbum[1].UpdateLayout();
            // renderUI(photoAlbum[0]);
        }
        
        //sample app
        void buildPhotoApp(ref Grid grid)
        {
            var text = new TextBlock
            {
                Text = "Photo App",
                FontSize = 100,
                FontWeight =
                FontWeights.Bold,
                Background = Brushes.White
            };
            grid.Children.Add(text);
            grid.Measure(new Size(grid.Width, grid.Height));
            grid.Arrange(new Rect(new Size(grid.Width, grid.Height)));
            grid.UpdateLayout();
        }

        //sample app
        void buildMapApp(ref Grid grid)
        {
            var text = new TextBlock
            {
                Text = "Map App",
                FontSize = 100,
                FontWeight =
                FontWeights.Bold,
                Background = Brushes.White
            };
            grid.Children.Add(text);
            grid.Measure(new Size(grid.Width, grid.Height));
            grid.Arrange(new Rect(new Size(grid.Width, grid.Height)));
            grid.UpdateLayout();
        }

        //sample app
        void buildSearchApp(ref Grid grid)
        {
            var text = new TextBlock
            {
                Text = "Search App",
                FontSize = 100,
                FontWeight =
                FontWeights.Bold,
                Background = Brushes.White
            };
            grid.Children.Add(text);
            grid.Measure(new Size(grid.Width, grid.Height));
            grid.Arrange(new Rect(new Size(grid.Width, grid.Height)));
            grid.UpdateLayout();
        }


        /*------------------BITMAP ROUTINES----------------------*/

        //Creates the RenderTargetBitmap of the canvas or the grid
        RenderTargetBitmap renderBitmap(FrameworkElement visual)
        {
            RenderTargetBitmap bitmap = new RenderTargetBitmap(
                 (int)screenInfo.flexDisplayWidth,
                 (int)screenInfo.flexDisplayHeight,
                 96,
                 96,
                 PixelFormats.Pbgra32);
            bitmap.Render(visual);
            return bitmap;
        }

        //Convert BitmapSource to Bitmap
        public static System.Drawing.Bitmap BitmapSourceToBitmap2(BitmapSource srs)
        {

            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(
                                          srs.PixelWidth,
                                          srs.PixelHeight,
                                          System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            System.Drawing.Imaging.BitmapData data = bmp.LockBits(
                                new System.Drawing.Rectangle(System.Drawing.Point.Empty, bmp.Size),
                                System.Drawing.Imaging.ImageLockMode.WriteOnly,
                                System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
            srs.CopyPixels(
                    Int32Rect.Empty,
                    data.Scan0,
                    data.Height * data.Stride,
                    data.Stride);
            bmp.UnlockBits(data);

            //Format24bppRgb
            System.Drawing.Bitmap clone = new System.Drawing.Bitmap(bmp.Width, bmp.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            using (System.Drawing.Graphics gr = System.Drawing.Graphics.FromImage(clone))
            {
                gr.DrawImage(bmp, new System.Drawing.Rectangle(0, 0, clone.Width, clone.Height));
                gr.Dispose();
            }
            return clone;
        }

        static byte[] ToByteArray(System.Drawing.Image image, System.Drawing.Imaging.ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, format);
                return ms.ToArray();
            }
        }

        //Save to PNG File
        void saveToPng(Display d)
        {
            var encoder = new PngBitmapEncoder();
            BitmapFrame frame = BitmapFrame.Create(display.bitmapForPush);
            encoder.Frames.Add(frame);
            using (var stream = File.Create(DateTime.Now.ToString("HH_mm_ss__fff_tt") + ".png"))
            {
                encoder.Save(stream);
            }
        }
        /*------------------END BITMAP ROUTINES----------------------*/


    }
}
