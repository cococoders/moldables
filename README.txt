Repo Created: 31st July 2013
Developers: Aneesh Tarun, David Parker

Description: Folder containing all moldables code

C# Project - BMPPush2: This is a stripped down version of the RenderMan project. This source code preceded RenderMan (and infact formed the base for creating the RenderMan code).

This project does the following: Create 1 canvas off-screen. Create UI off-screen. The created UI can be pushed to three outputs: On-screen Canvas, Flex Display, to a PNG file. All of functions can be controlled by keyboard inputs for debugging. Modify "MainWindow_KeyDown" to allow for more control of the program.